#include <stdio.h>

#define NLines 7
#define NCols 7
#define MAX_BRIGHT 5
#if NLines > NCols
#define smaller NCols
#else
#define smaller NLines
#endif

int arrayPat[NLines][NCols][3];
int vectorPat[NLines * NCols][3];
int color[3] = {127, 0, 127};

void printMatrix()
{
	printf("\n\n");
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			for (int z = 0; z < 3; z++)
			{
				printf("%03d", vectorPat[x + (y * NCols)][z]);
				if (z < 2)
					printf(",");
			}
			printf(" ");
		}
		printf("\n");
	}
}

void drawCircle(int *color_steps)
{
	float z = ((float)smaller / 2);
	int coordsX = 0, coordsY = 0, coordsV = 0;
	for (int y = -(NLines / 2); y <= (NLines / 2); y++)
	{
		for (int x = -(NCols / 2); x <= (NCols / 2); x++)
		{
			coordsX = (NCols / 2) + x;
			coordsY = (NLines / 2) + y;
			coordsV = (x + (NCols / 2)) + ((y + (NLines / 2)) * NCols);
			for (int i = 0; i < MAX_BRIGHT; i++)
			{
				float perc = 1 - ((float)i / MAX_BRIGHT);
				// printf("%0.1f\n", perc);
				if ((x * x) + (y * y) <= (z * z) * (perc))
				{
					for (int z = 0; z < 3; z++)
					{
						arrayPat[coordsX][coordsY][z] = color[z] + i * color_steps[z];
						vectorPat[coordsV][z] = color[z] + i * color_steps[z];
					}
				}
			}
		}
	}
}

void move(int newX, int newY)
{
	int tempArray[NCols][NLines][3];
	int tempVector[NCols * NLines][3];
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			for (int z = 0; z < 3; z++)
			{
				tempArray[x][y][z] = arrayPat[x][y][z];
				tempVector[x + (y * NCols)][z] = vectorPat[x + (y * NCols)][z];
			}
		}
	}

	int tempX, tempY;
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			if ((x - newX) < 0)
				tempX = NCols - (x + newX);
			else if ((x - newX) >= NCols)
				tempX = (x - newX) - NCols;
			else
				tempX = -newX + x;

			if ((y - newY) < 0)
				tempY = NLines - (y + newY);
			else if ((y - newY) >= NLines)
				tempY = (y - newY) - NLines;
			else
				tempY = -newY + y;
			for (int z = 0; z < 3; z++)
			{
				arrayPat[x][y][z] = tempArray[tempX][tempY][z];
				vectorPat[x + (y * NCols)][z] = tempVector[tempX + (tempY * NCols)][z];
			}
		}
	}
}

/* void moveBrightness(int inc)
{
	int tempArray[NCols][NLines];
	int tempVector[NCols * NLines];
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			tempArray[x][y] = arrayPat[x][y];
			tempVector[x + (y * NCols)] = vectorPat[x + (y * NCols)];
		}
	}
	int temp;
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			temp = arrayPat[x][y] + inc;
			if (temp < 0)
				temp = MAX_BRIGHT + temp + 1;
			else if (temp > MAX_BRIGHT)
				temp = temp - MAX_BRIGHT - 1;
			arrayPat[x][y] = temp;
		}
	}
} */

int main()
{
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			for (int z = 0; z < 3; z++)
			{
				arrayPat[x][y][z] = 0;
				vectorPat[x + (y * NCols)][z] = 0;
			}
		}
	}
	int color[] = {5, 5, 5};
	drawCircle(color);
	printMatrix();
	move(1, 0);
	//moveBrightness(-2);
	//move(-1, 0);
	printMatrix();
	return 0;
}