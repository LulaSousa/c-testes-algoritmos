#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <string.h>

#define SIZE 45
#define SIZEL SIZE - 1
#define NLEDS SIZE *SIZE
#define WS0 0
#define WS1 1
#define MAX_BRIGHT 5

#define SWAP(x, y)       \
	do                   \
	{                    \
		(x) = (x) ^ (y); \
		(y) = (x) ^ (y); \
		(x) = (x) ^ (y); \
	} while (0)

char vectorPat[SIZE * SIZE];

const int font4x6[96][2] = {
	{0x00, 0x00}, /*SPACE*/
	{0x49, 0x08}, /*'!'*/
	{0xb4, 0x00}, /*'"'*/
	{0xbe, 0xf6}, /*'#'*/
	{0x7b, 0x7a}, /*'$'*/
	{0xa5, 0x94}, /*'%'*/
	{0x55, 0xb8}, /*'&'*/
	{0x48, 0x00}, /*'''*/
	{0x29, 0x44}, /*'('*/
	{0x44, 0x2a}, /*')'*/
	{0x15, 0xa0}, /*'*'*/
	{0x0b, 0x42}, /*'+'*/
	{0x00, 0x50}, /*','*/
	{0x03, 0x02}, /*'-'*/
	{0x00, 0x08}, /*'.'*/
	{0x25, 0x90}, /*'/'*/
	{0x76, 0xba}, /*'0'*/
	{0x59, 0x5c}, /*'1'*/
	{0xc5, 0x9e}, /*'2'*/
	{0xc5, 0x38}, /*'3'*/
	{0x92, 0xe6}, /*'4'*/
	{0xf3, 0x3a}, /*'5'*/
	{0x73, 0xba}, /*'6'*/
	{0xe5, 0x90}, /*'7'*/
	{0x77, 0xba}, /*'8'*/
	{0x77, 0x3a}, /*'9'*/
	{0x08, 0x40}, /*':'*/
	{0x08, 0x50}, /*';'*/
	{0x2a, 0x44}, /*'<'*/
	{0x1c, 0xe0}, /*'='*/
	{0x88, 0x52}, /*'>'*/
	{0xe5, 0x08}, /*'?'*/
	{0x56, 0x8e}, /*'@'*/
	{0x77, 0xb6}, /*'A'*/
	{0x77, 0xb8}, /*'B'*/
	{0x72, 0x8c}, /*'C'*/
	{0xd6, 0xba}, /*'D'*/
	{0x73, 0x9e}, /*'E'*/
	{0x73, 0x92}, /*'F'*/
	{0x72, 0xae}, /*'G'*/
	{0xb7, 0xb6}, /*'H'*/
	{0xe9, 0x5c}, /*'I'*/
	{0x64, 0xaa}, /*'J'*/
	{0xb7, 0xb4}, /*'K'*/
	{0x92, 0x9c}, /*'L'*/
	{0xbe, 0xb6}, /*'M'*/
	{0xd6, 0xb6}, /*'N'*/
	{0x56, 0xaa}, /*'O'*/
	{0xd7, 0x92}, /*'P'*/
	{0x76, 0xee}, /*'Q'*/
	{0x77, 0xb4}, /*'R'*/
	{0x71, 0x38}, /*'S'*/
	{0xe9, 0x48}, /*'T'*/
	{0xb6, 0xae}, /*'U'*/
	{0xb6, 0xaa}, /*'V'*/
	{0xb6, 0xf6}, /*'W'*/
	{0xb5, 0xb4}, /*'X'*/
	{0xb5, 0x48}, /*'Y'*/
	{0xe5, 0x9c}, /*'Z'*/
	{0x69, 0x4c}, /*'['*/
	{0x91, 0x24}, /*'\'*/
	{0x64, 0x2e}, /*']'*/
	{0x54, 0x00}, /*'^'*/
	{0x00, 0x1c}, /*'_'*/
	{0x44, 0x00}, /*'`'*/
	{0x0e, 0xae}, /*'a'*/
	{0x9a, 0xba}, /*'b'*/
	{0x0e, 0x8c}, /*'c'*/
	{0x2e, 0xae}, /*'d'*/
	{0x0e, 0xce}, /*'e'*/
	{0x56, 0xd0}, /*'f'*/
	{0x55, 0x3B}, /*'g'*/
	{0x93, 0xb4}, /*'h'*/
	{0x41, 0x44}, /*'i'*/
	{0x41, 0x51}, /*'j'*/
	{0x97, 0xb4}, /*'k'*/
	{0x49, 0x44}, /*'l'*/
	{0x17, 0xb6}, /*'m'*/
	{0x1a, 0xb6}, /*'n'*/
	{0x0a, 0xaa}, /*'o'*/
	{0xd6, 0xd3}, /*'p'*/
	{0x76, 0x67}, /*'q'*/
	{0x17, 0x90}, /*'r'*/
	{0x0f, 0x38}, /*'s'*/
	{0x9a, 0x8c}, /*'t'*/
	{0x16, 0xae}, /*'u'*/
	{0x16, 0xba}, /*'v'*/
	{0x16, 0xf6}, /*'w'*/
	{0x15, 0xb4}, /*'x'*/
	{0xb5, 0x2b}, /*'y'*/
	{0x1c, 0x5e}, /*'z'*/
	{0x6b, 0x4c}, /*'{'*/
	{0x49, 0x48}, /*'|'*/
	{0xc9, 0x5a}, /*'}'*/
	{0x54, 0x00}, /*'~'*/
	{0x56, 0xe2}  /*''*/
};

unsigned char getFontLine(unsigned char data, int line_num)
{
	const int index = (data - 32);
	unsigned char pixel = 0;
	if ((font4x6[index][1] & 1) == 1)
		line_num -= 1;
	if (line_num == 0)
	{
		pixel = (font4x6[index][0]) >> 4;
	}
	else if (line_num == 1)
	{
		pixel = (font4x6[index][0]) >> 1;
	}
	else if (line_num == 2)
	{
		// Split over 2 bytes
		return (((font4x6[index][0]) & 0x03) << 2) | (((font4x6[index][1]) & 0x02));
	}
	else if (line_num == 3)
	{
		pixel = (font4x6[index][1]) >> 4;
	}
	else if (line_num == 4)
	{
		pixel = (font4x6[index][1]) >> 1;
	}
	return pixel & 0xE;
}

void printBuffer()
{
	printf("\n\n");
	for (int y = 0; y < SIZE; y++)
	{
		for (int x = 0; x < SIZE; x++)
		{
			printf("%c ", vectorPat[x + (y * SIZE)]);
		}
		printf("\n");
	}
}

void drawLetter(char letter, int px, int py)
{
	unsigned int fill;
	unsigned int val = 0;
	for (int y = py, l = 0; y <= (py + 5); y++, l++)
	{
		fill = getFontLine(letter, l);
		for (int x = px, aX = 3; x <= (px + 3); x++, aX--)
		{
			// val = fill & (1u << aX) ? 120 : 46;
			val = fill & (1u << aX) ? 120 : 32;
			vectorPat[x + (y * SIZE)] = val;
		}
	}
}

void drawString(char *frase, int x, int y)
{
	int posX = 1, posY = 1, nLine = 0, posI = 0;
	for (size_t i = 0; i < strlen(frase); i++)
	{
		posI = (i * 3);
		nLine = posI / (SIZE - 3);
		posY = (nLine * 6) + 1;
		posX = (posI - (nLine * (SIZE - 3))) + 1;
		printf("%d  %d\n", posX, posY);
		drawLetter(frase[i], posX, posY);
	}
}

void drawStringLine(char *frase, int x, int y)
{
	int posX = 1, posY = 1, nLine = 0, posI = 0;
	for (size_t i = 0; i < strlen(frase); i++)
	{
		posI = (i * 3);
		nLine = posI / (SIZE - 3);
		posX = posI + 1;
		// printf("%d  %d\n", posX, posY);
		if ((posX + 3) > SIZE)
			break;
		drawLetter(frase[i], posX, posY);
	}
}

void drawScrollingString(char *frase, uint8_t pos, uint8_t letter, uint8_t offX, uint8_t offY)
{
	int8_t posX, posY;
	uint8_t fill, val, limit;
	for (uint8_t i = letter, idx = 0; i < strlen(frase); i++, idx++)
	{
		posX = (idx * 3) + offX;
		posY = offY;

		for (uint8_t y = posY, l = 0; y <= (posY + 5); y++, l++)
		{
			if (y >= (SIZE - offY))
				break;
			fill = getFontLine(frase[i], l);
			for (int8_t x = posX - pos, aX = 3; x < (posX + 3) - pos; x++, aX--)
			{
				if (x >= (SIZE - offX))
					break;
				val = fill & (1u << aX) ? 120 : 32;
				if (x >= offX)
					vectorPat[x + (y * SIZE)] = val;
			}
		}
	}
	printBuffer();
	for (int yy = 0; yy < SIZE; yy++)
	{
		for (int xx = 0; xx < SIZE; xx++)
		{
			vectorPat[xx + (yy * SIZE)] = 46;
		}
	}
}

void drawScrollingLine(char *frase, int x, int y)
{
	for (uint8_t i = 0; i < strlen(frase); i++)
	{
		for (uint8_t j = 0; j < 3; j++)
		{
			printf("%d,%d \n", i, j);
			drawScrollingString(frase, j, i, x, y);
		}
	}
}

void move(int newX, int newY)
{
	int tempVector[SIZE * SIZE];
	// newX = -newX;
	// newY = -newY;
	int tempX, tempY;
	for (int y = 0; y < SIZE; y++)
	{
		for (int x = 0; x < SIZE; x++)
		{
			tempVector[x + (y * SIZE)] = vectorPat[x + (y * SIZE)];
		}
	}
	printf("\n");
	for (int y = 0; y < SIZE; y++)
	{
		if ((y - newY) < 0)
		{
			tempY = SIZE - (-y + newY);
			printf("\nY %d %d \n\n", y, tempY);
		}
		else if ((y - newY) >= SIZE)
		{
			tempY = (y - newY) - SIZE;
			printf("\nY %d %d \n\n", y, tempY);
		}
		else
			tempY = y - newY;

		// printf("\nY     %d  %d     %d\n\n", y, newY, tempY);
		for (int x = 0; x < SIZE; x++)
		{
			if ((x - newX) < 0)
			{
				tempX = SIZE - (-x + newX);
				printf("X %d %d \n", x, tempX);
			}
			else if ((x + newX) >= SIZE)
			{
				tempX = (x + newX) - SIZE;
				printf("X %d %d \n", x, tempX);
			}
			else
				tempX = x + newX;

			// printf("%d  %d     %d\n", y, newY, tempY);
			// printf("X     %d  %d     %d\n", x, newX, tempX);
			vectorPat[x + (y * SIZE)] = tempVector[tempX + (tempY * SIZE)];
		}
	}
}

void rotate()
{
	int tempVector[SIZE * SIZE];
	uint16_t x, y;
	for (y = 0; y < SIZE; y++)
	{
		for (x = 0; x < SIZE; x++)
		{
			tempVector[x + (y * SIZE)] = vectorPat[x + (y * SIZE)];
		}
	}
	printf("\n");

	for (y = 0; y < SIZE; y++)
	{
		for (x = 0; x < SIZE; x++)
		{
			vectorPat[-y - 1 + (x + 1) * SIZE] = tempVector[x + (y * SIZE)];
		}
	}
}

void rotate45()
{
	int tempVector[SIZE * SIZE];
	int x1, x2, z, middleH, upperr, upperL, upperOff, upperNOff, upperLOff, half = SIZE / 2, halfH = half + 1, halfL = half - 1, zOff;

	// for (int y = 0; y < SIZE; y++)
	// {
	for (x2 = 0; x2 < SIZE; x2++)
	{
		for (x1 = 0; x1 < SIZE; x1++)
		{
			tempVector[x1 + (x2 * SIZE)] = vectorPat[x1 + (x2 * SIZE)];
		}
	}

	// }
	for (int z = 0; z < half; z++) //1; z++)
	{
		// for (int z = -1; z < 1; z++)
		// {
		upperr = SIZE - z;
		middleH = halfH - z;
		upperL = upperr - 1;
		upperNOff = (SIZE - z - 1) * SIZE;
		upperOff = SIZE * SIZE;
		upperLOff = upperL * SIZE;

		zOff = z * SIZE;

		for (x1 = 0; x1 < middleH; x1++)
		{
			// vectorPat[upperL + ((x1 + half) * SIZE)] = tempVector[upperL + ((x1 + z) * SIZE)];
			// vectorPat[upperL + ((x1 + z) * SIZE)] = tempVector[(x1 + half) + zOff];
			// vectorPat[(x1 + half) + zOff] = tempVector[(x1 + z) + zOff];
			// vectorPat[(x1 + z) + zOff] = tempVector[z + ((half - x1) * SIZE)];
			// vectorPat[z + ((half - x1) * SIZE)] = tempVector[z + ((upperL - x1) * SIZE)];
			// vectorPat[z + ((upperL - x1) * SIZE)] = tempVector[(half - x1) + upperLOff];
			// vectorPat[(half - x1) + upperLOff] = tempVector[-x1 + (SIZE - z - 1) * (SIZE + 1)];
			// vectorPat[-x1 + (SIZE - z - 1) * (SIZE + 1)] = tempVector[upperL + ((x1 + half) * SIZE)];
			vectorPat[-z - SIZEL + ((x1 + (SIZE / 2)) * SIZE)] = tempVector[-z - 1 + ((z + x1 + 1) * SIZE)];
			vectorPat[-z - 1 + ((z + x1 + 1) * SIZE)] = tempVector[x1 + (SIZE / 2) + (z * SIZE)];
			vectorPat[x1 + (SIZE / 2) + (z * SIZE)] = tempVector[(z + x1) + (z * SIZE)];
			vectorPat[(z + x1) + (z * SIZE)] = tempVector[z + ((-x1 + (SIZE / 2)) * SIZE)];
			vectorPat[z + ((-x1 + (SIZE / 2)) * SIZE)] = tempVector[z + ((-z - x1 + SIZE - 1) * SIZE)];
			vectorPat[z + ((-z - x1 + SIZE - 1) * SIZE)] = tempVector[-x1 - (SIZE / 2) - 1 + (-z + SIZE) * SIZE];
			vectorPat[((SIZE / 2) - x1) + (SIZE - z - 1) * SIZE] = tempVector[-z - x1 - 1 + (SIZE - z) * SIZE];
			vectorPat[-z - x1 - 1 + (SIZE - z) * SIZE] = tempVector[-z - 1 + ((x1 + (SIZE / 2) + 1) * SIZE)];
		}
		printf("\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// {
		// 	vectorPat[upperL + (x1 * SIZE)] = tempVector[upperL + (x2 * SIZE)];
		// 	vectorPat[upperL + (x2 * SIZE)] = tempVector[x1 + zOff];
		// 	vectorPat[x1 + zOff] = tempVector[x2 + zOff];
		// 	vectorPat[x2 + zOff] = tempVector[z + ((SIZE - 1 - x1) * SIZE)];
		// }
		// for (x1 = upperL, x2 = half; x1 > halfL; x1--, x2--)
		// {
		// 	vectorPat[z + (x2 * SIZE)] = tempVector[z + (x1 * SIZE)];
		// 	vectorPat[z + (x1 * SIZE)] = tempVector[x2 + upperLOff];
		// 	vectorPat[x2 + upperLOff] = tempVector[x1 + upperLOff];
		// 	vectorPat[x1 + upperLOff] = tempVector[((SIZE - x2) * SIZE) - 1 - z];
		// }
		// }

		// printf("1\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", upperL + (x1 * SIZE), upperL + ((x1 - half + z) * SIZE));

		// printf("2\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", upperL + (x2 * SIZE), x1 + zOff);

		// printf("3\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", x1 + zOff, x2 + zOff);

		// printf("4\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", x2 + zOff, z + ((SIZE - 1 - x1) * SIZE));

		// printf("5\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", z + ((SIZE - 1 - x1) * SIZE), z + ((SIZE - 1 - x2) * SIZE));

		// printf("6\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", z + ((SIZE - 1 - x2) * SIZE), (half + z - x2) + upperLOff);

		// printf("7\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", (half + z - x2) + upperLOff, (SIZE - 1 - x2) + ((SIZE - z - 1) * SIZE));

		// printf("8\n");
		// for (x1 = half, x2 = z; x1 < upperr; x1++, x2++)
		// 	printf("%d,%d\n", (SIZE - 1 - x2) + ((SIZE - z - 1) * SIZE), upperL + (x1 * SIZE));
	}
}

void rotate45v2()
{
	int tempVector[SIZE * SIZE];
	int x1, x2, z;
	// for (int y = 0; y < SIZE; y++)
	// {
	for (x2 = 0; x2 < SIZE; x2++)
	{
		for (x1 = 0; x1 < SIZE; x1++)
		{
			tempVector[x1 + (x2 * SIZE)] = vectorPat[x1 + (x2 * SIZE)];
		}
	}

	// }
	printf("\n");
	for (int z = 0; z < (SIZE / 2); z++) //1; z++)
	{
		// for (int z = -1; z < 1; z++)
		// {
		printf("1\n");
		for (x1 = (SIZE / 2), x2 = z; x1 < SIZE - z; x1++, x2++)
		{
			printf("%d,%d\n", (SIZE - 1 - z) + (x2 * SIZE), x1 + (z * SIZE));
			vectorPat[(SIZE - 1 - z) + (x2 * SIZE)] = tempVector[x1 + (z * SIZE)];
		}
		printf("2\n");
		for (x1 = z, x2 = (SIZE / 2); x1 < (SIZE / 2) + 1; x1++, x2++)
		{
			printf("%d,%d\n", x2 + (z * SIZE), x1 + (z * SIZE));
			vectorPat[x2 + (z * SIZE)] = tempVector[x1 + (z * SIZE)];
		}
		printf("3\n");
		for (x1 = (SIZE / 2), x2 = z; x1 > z - 1; x1--, x2++)
		{
			printf("%d,%d\n", x2 + (z * SIZE), z + (x1 * SIZE));
			vectorPat[x2 + (z * SIZE)] = tempVector[z + (x1 * SIZE)];
		}
		printf("4\n");
		for (x1 = (SIZE - 1 - z), x2 = (SIZE / 2); x1 > (SIZE / 2) - 1; x1--, x2--)
		{
			printf("%d,%d\n", z + (x2 * SIZE), z + (x1 * SIZE));
			vectorPat[z + (x2 * SIZE)] = tempVector[z + (x1 * SIZE)];
		}
		printf("5\n");
		for (x1 = (SIZE / 2), x2 = (SIZE - 1 - z); x1 > z - 1; x1--, x2--)
		{
			printf("%d,%d\n", (x2 * SIZE) + z, x1 + ((SIZE - 1 - z) * SIZE));
			vectorPat[z + (x2 * SIZE)] = tempVector[x1 + ((SIZE - 1 - z) * SIZE)];
		}
		printf("6\n");
		for (x1 = (SIZE - 1 - z), x2 = (SIZE / 2); x1 > (SIZE / 2) - 1; x1--, x2--)
		{
			printf("%d,%d\n", x2 + ((SIZE - 1 - z) * SIZE), x1 + ((SIZE - 1 - z) * SIZE));
			vectorPat[x2 + ((SIZE - 1 - z) * SIZE)] = tempVector[x1 + ((SIZE - 1 - z) * SIZE)];
		}
		printf("7\n");
		for (x1 = (SIZE / 2), x2 = (SIZE - 1) - z; x1 < SIZE - z; x1++, x2--)
		{
			printf("%d,%d\n", x2 + ((SIZE - 1 - z) * SIZE), (SIZE - 1 - z) + (x1 * SIZE));
			vectorPat[x2 + ((SIZE - 1 - z) * SIZE)] = tempVector[(SIZE - 1 - z) + (x1 * SIZE)];
		}
		printf("8\n");
		for (x1 = z, x2 = (SIZE / 2); x1 < (SIZE / 2) + 1; x1++, x2++)
		{
			printf("%d,%d\n", (SIZE - 1 - z) + (x2 * SIZE), (SIZE - 1 - z) + (x1 * SIZE));
			vectorPat[(SIZE - 1 - z) + (x2 * SIZE)] = tempVector[(SIZE - 1 - z) + (x1 * SIZE)];
		}
		// }
	}
}

/* void moveBrightness(int inc)
{
	int tempArray[SIZE][SIZE];
	int tempVector[SIZE * SIZE];
	for (int y = 0; y < SIZE; y++)
	{
		for (int x = 0; x < SIZE; x++)
		{
			tempArray[x][y] = arrayPat[x][y];
			tempVector[x + (y * SIZE)] = vectorPat[x + (y * SIZE)];
		}
	}
	int temp;
	for (int y = 0; y < SIZE; y++)
	{
		for (int x = 0; x < SIZE; x++)
		{
			temp = arrayPat[x][y] + inc;
			if (temp < 0)
				temp = MAX_BRIGHT + temp + 1;
			else if (temp > MAX_BRIGHT)
				temp = temp - MAX_BRIGHT - 1;
			arrayPat[x][y] = temp;
		}
	}
} */

int main()
{
	for (int y = 0; y < SIZE; y++)
	{
		for (int x = 0; x < SIZE; x++)
		{
			vectorPat[x + (y * SIZE)] = 46;
		}
	}

	drawString("A que horas queres jantar?", 0, 0);
	// drawScrollingLine("123456", 0, 0);
	rotate45v2();
	printBuffer();
	// rotate45();
	// printBuffer();
	// rotate45();
	// printBuffer();
	// rotate45();
	// move(-1, -1);
	//printMatrix();
	// move(-1, 2);
	// printMatrix();
	// move(0, 2);
	// //moveBrightness(-2);
	// //move(-1, 0);
	// int len = 49;
	// int mod;
	// printf("\n\n");
	// while (len)
	// {
	// 	mod = len % SIZE;
	// 	if (mod == 0)
	// 		printf("\n");
	// 	printf("%d   %d\n", len, mod);
	// 	len--;
	// }
	return 0;
}