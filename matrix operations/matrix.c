#include <stdio.h>

#define NLines 7
#define NCols 7
#define MAX_BRIGHT 5
#if NLines > NCols
#define smaller NCols
#else
#define smaller NLines
#endif

int arrayPat[NLines][NCols];
int vectorPat[NLines * NCols];

void printMatrix()
{
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			printf("%d", arrayPat[x][y]);
			printf(" ");
		}
		printf("\n");
	}
	printf("\n\n");
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			printf("%d", vectorPat[x + (y * NCols)]);
			printf(" ");
		}
		printf("\n");
	}
}

void drawCircle()
{
	float z = smaller / 2;
	int coordsX = 0, coordsY = 0, coordsV = 0;
	for (int y = -(NLines / 2); y <= (NLines / 2); y++)
	{
		for (int x = -(NCols / 2); x <= (NCols / 2); x++)
		{
			coordsX = (NCols / 2) + x;
			coordsY = (NLines / 2) + y;
			coordsV = (x + (NCols / 2)) + ((y + (NLines / 2)) * NCols);
			for (int i = 0; i < MAX_BRIGHT; i++)
			{
				float perc = 1 - ((float)i / MAX_BRIGHT);
				printf("%0.1f\n", perc);
				if ((x * x) + (y * y) <= (z * z) * (perc))
				{
					arrayPat[coordsX][coordsY] = i + 1;
					vectorPat[coordsV] = i + 1;
				}
			}
		}
	}
}

void move(int newX, int newY)
{
	int tempArray[NCols][NLines];
	int tempVector[NCols * NLines];
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			tempArray[x][y] = arrayPat[x][y];
			tempVector[x + (y * NCols)] = vectorPat[x + (y * NCols)];
		}
	}

	int tempX, tempY;
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			if ((x - newX) < 0)
				tempX = NCols - (x + newX);
			else if ((x - newX) >= NCols)
				tempX = (x - newX) - NCols;
			else
				tempX = -newX + x;

			if ((y - newY) < 0)
				tempY = NLines - (y + newY);
			else if ((y - newY) >= NLines)
				tempY = (y - newY) - NLines;
			else
				tempY = -newY + y;
			arrayPat[x][y] = tempArray[tempX][tempY];
			vectorPat[x + (y * NCols)] = tempVector[tempX + (tempY * NCols)];
		}
	}
}

void moveBrightness(int inc)
{
	int tempArray[NCols][NLines];
	int tempVector[NCols * NLines];
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			tempArray[x][y] = arrayPat[x][y];
			tempVector[x + (y * NCols)] = vectorPat[x + (y * NCols)];
		}
	}
	int temp;
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			temp = arrayPat[x][y] + inc;
			if (temp < 0)
				temp = MAX_BRIGHT + temp + 1;
			else if (temp > MAX_BRIGHT)
				temp = temp - MAX_BRIGHT - 1;
			arrayPat[x][y] = temp;
		}
	}
}

int main()
{
	for (int y = 0; y < NLines; y++)
	{
		for (int x = 0; x < NCols; x++)
		{
			arrayPat[x][y] = 0;
			vectorPat[x + (y * NCols)] = 0;
		}
	}
	drawCircle();
	//move(1, 0);
	moveBrightness(-2);
	//move(-1, 0);
	printMatrix();
	return 0;
}